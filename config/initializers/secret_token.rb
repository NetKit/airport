# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
WebAirport3::Application.config.secret_key_base = 'd3e9e166df9eb0400808d44ac06971002ab6bae0b1220df65e24045c6d71ecbe1f00158bac5147177d78bad5a46b48e5294223db744c439b871b5cb7f6b6ab55'

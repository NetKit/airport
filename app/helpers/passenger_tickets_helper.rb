module PassengerTicketsHelper
  def select_free_seats(name, selected = nil, flight_id)
    select_tag(name, options_for_select(
    Ticket.joins('LEFT OUTER JOIN passenger_tickets ON tickets.id = passenger_tickets.ticket_id LEFT OUTER JOIN flights ON flights.id = tickets.flight_id').where('passenger_tickets.ticket_id IS ? AND flights.id = ?', nil, flight_id).load.map{ |x| [x.seat_num] } ,
    [selected]))
  end
  def select_type(name, selected = nil)
    select_tag(name, options_for_select(["Детский","Взрослый"], [selected]))
  end
end

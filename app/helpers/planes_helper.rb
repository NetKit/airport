module PlanesHelper
  def select_ptype(name, selected = nil)
    select_tag(name, options_for_select(["Грузовой","Пассажирский"], [selected]))
  end
end

module GatesHelper
  def select_terminal(name, selected = nil)
    select_tag(name, options_for_select(
        Terminal.order('code').load.map{ |x| [x.code, x.id] } + [['', nil]],
        [selected]))
  end
end

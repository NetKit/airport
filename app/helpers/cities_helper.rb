module CitiesHelper
  def select_country(name, selected = nil)
    select_tag(name, options_for_select(
        Country.order('code').load.map{ |x| [x.name, x.id] } + [['', nil]],
        [selected]))
  end
end

module CountriesHelper
  def select_language(name, selected = nil)
    select_tag(name, options_for_select(
        Language.order('name').load.map{ |x| [x.name, x.id] } + [['', nil]],
        [selected]))
  end
end

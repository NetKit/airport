module PassengersHelper
  def select_sex(name, selected = nil)
    select_tag(name, options_for_select(["м","ж"], [selected]))
  end

  def select_passenger(name, selected = nil)
    p = Passenger.pluck(:id).to_a
    select_tag(name, options_for_select(p, [selected]))
  end
end

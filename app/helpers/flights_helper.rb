module FlightsHelper
  def select_terminal(name, selected = nil)
    select_tag(name, options_for_select(
        Terminal.order('code').load.map{ |x| [x.code, x.id] } + [['', nil]],
        [selected]))
  end

  def select_registration_desk(name, selected = nil)
    select_tag(name, options_for_select(
        RegistrationDesk.order('code').load.map{ |x| [x.code, x.id] } + [['', nil]],
        [selected]))
  end

  def select_gate(name, selected = nil)
    select_tag(name, options_for_select(
        Gate.order('code').load.map{ |x| [x.code, x.id] } + [['', nil]],
        [selected]))
  end

  def select_airport(name, selected = nil)
    select_tag(name, options_for_select(
        Airport.includes(:city).order('code').load.map{ |x| ["#{x.code} #{x.city.name}", x.id] } + [['', nil]],
        [selected]))
  end

  def select_company(name, selected = nil)
    select_tag(name, options_for_select(
        Company.order('name').load.map{ |x| [x.name, x.id] } + [['', nil]],
        [selected]))
  end

  def select_flight_type(name, selected = nil)
    select_tag(name, options_for_select(["Грузоперевозки", "Пассажирский"], [selected]))
  end

  def select_registered_passengers
    Passenger.includes(passenger_tickets: {ticket: [:flight]}).where(flights: {id: self.id})
  end
end

module AirportsHelper
  def select_city(name, selected = nil)
    select_tag(name, options_for_select(
        City.order('code').load.map{ |x| ["#{x.name} (#{x.country.name})", x.id] } + [['', nil]],
        [selected]))
  end
end

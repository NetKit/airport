module TerminalsHelper
  def select_airport(name, selected = nil)
    select_tag(name, options_for_select(
        Airport.order('code').load.map{ |x| [x.code, x.id] } + [['', nil]],
        [selected]))
  end
end

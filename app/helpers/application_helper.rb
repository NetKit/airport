module ApplicationHelper
  # Очень плохой способ задания меню, но, зато, самый простой
  def drop1(user)
    result = [
      link_to('Табло прилёта', controller: :flights, action: :index,
      is_departure: false),
      link_to('Табло вылета', controller: :flights, action: :index,
      is_departure: true),
      link_to('Все рейсы', controller: :flights, action: :index)
    ]
  end

  def menu(user)
    result = []

    unless user.nil?
      if user.roles.include?(ApplicationController::OPERATOR_ROLE)
        result += [
          link_to('Иностранные языки', controller: :languages,
           action: :index),
          link_to('Страны и города', controller: :countries,
           action: :index),
          link_to('Аэропорты', controller: :airports,
           action: :index),
          link_to('Терминалы, регистрационные стойки и выходы',
            controller: :terminals, action: :index),
          link_to('Авиакомпании', controller: :companies,
           action: :index),
          link_to('Пассажиры', controller: :passengers, action: :index),
          link_to('Билеты', controller: :passenger_tickets, action: :index)
        ]
      elsif user.roles.include?(ApplicationController::ADMIN_ROLE)
        result += [
          link_to('Пользователи', controller: :users,
           action: :index),
          link_to('Роли', controller: :roles,
           action: :index)
        ]
      end
    end
    result
  end
end

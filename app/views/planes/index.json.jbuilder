json.array!(@planes) do |plane|
  json.extract! plane, :id, :num, :model, :floors, :rows, :buis_st, :prem_st, :eco_st
  json.url plane_url(plane, format: :json)
end

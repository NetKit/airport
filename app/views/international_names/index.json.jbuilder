json.array!(@international_names) do |international_name|
  json.extract! international_name, :name, :language_id, :term_id, :term_type
  json.url international_name_url(international_name, format: :json)
end

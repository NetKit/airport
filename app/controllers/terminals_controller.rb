class TerminalsController < ApplicationController
  before_action :set_terminal, only: [:show, :edit, :update, :destroy]

  # GET /terminals
  def index
    @terminals = Terminal.includes(:gates, :registration_desks).load
  end

  # GET /terminals/show/1
  def show
  end

  # GET /terminals/new
  def new
    @terminal = Terminal.new
  end

  # GET /terminals/edit/1
  def edit
  end

  # POST /terminals/create
  def create
    @terminal = Terminal.new(terminal_params)
    
    respond_to do |format|
      if @terminal.save
        format.html { redirect_to action: :show, id: @terminal.id, notice: 'Терминал успешно создан.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /terminals/update
  def update
    @terminal.attributes = terminal_params
    
    respond_to do |format|
      if @terminal.save()
        format.html { redirect_to action: :show, id: @terminal.id, notice: 'Терминал успешно обновлён.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /terminals/destroy/1
  def destroy
    @terminal.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_terminal
      @terminal = Terminal.includes(:gates, :registration_desks).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def terminal_params
      params.require(:terminal).permit(:code)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # отвечает на /users/logout
  def logout
    @ouser = nil
    session[:user] = nil
    redirect_to(root_path)
  end

  # GET /users/index
  def index
    @users = User.includes(:roles).load
  end

  # GET /users/show/1
  def show
  end

  # GET /users/new
  def new
    @ouser = User.new
  end

  # GET /users/edit/1
  def edit
  end

  # POST /users/create
  def create
    @ouser = User.new(user_params)
    roles = Role.where(id: [params['user']['role'].map{ |x| x.to_i }]).load
    @ouser.roles = roles

    respond_to do |format|
      if @ouser.save
        format.html { redirect_to action: :show, id: @ouser.id, notice: 'Пользователь успешно создан.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /users/update
  def update
    @ouser.attributes = user_params
    roles = Role.where(id: [params['user']['role'].map{ |x| x.to_i }]).load
    @ouser.roles = roles

    respond_to do |format|
      if @ouser.save
        format.html { redirect_to action: :show, id: @ouser.id, notice: 'Пользователь успешно обновлён.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /users/destroy/1
  def destroy
    @ouser.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @ouser = User.includes(:roles).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:login, :info, :password)
    end

    def permit?(aname)
      return true if aname == 'logout'
      return true if !@user.nil? and @user.roles.include?(ADMIN_ROLE)
      return false
    end
end

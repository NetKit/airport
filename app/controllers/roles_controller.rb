class RolesController < ApplicationController
  before_action :set_role, only: [:show, :edit, :update, :destroy]

  # GET /roles/index
  def index
    @roles = Role.includes(:users).all
  end

  # GET /roles/show/1
  def show
  end

  # GET /roles/new
  def new
    @role = Role.new
  end

  # GET /roles/edit/1
  def edit
  end

  # POST /roles/create
  def create
    @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
        format.html { redirect_to action: :show, id: @role.id, notice: 'Роль успешно создана.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /roles/update
  def update
    respond_to do |format|
      if @role.update(role_params)
        format.html { redirect_to action: :show, id: @role.id, notice: 'Роль успешно обновлена.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /roles/destroy/1
  def destroy
    @role.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:name, :info)
    end

    def permit?(aname)
      return true if !@user.nil? and @user.roles.include?(ADMIN_ROLE)
      return false
    end
end

class GatesController < ApplicationController
  before_action :set_gate, only: [:show, :edit, :update, :destroy]

  # GET /gates/show/1
  def show
  end

  # GET /gates/new
  def new
    @gate = Gate.new
    terminal = Terminal.where(id: params[:terminal_id]).first
    @gate.terminal = terminal
  end

  # GET /gates/edit/1
  def edit
  end

  # POST /gates/create
  def create
    @gate = Gate.new(gate_params)
    terminal = Terminal.where(id: gate_params[:terminal_id]).first
    @gate.terminal = terminal

    respond_to do |format|
      if @gate.save
        format.html { redirect_to action: :show, id: @gate.id, notice: 'Выход успешно создан.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /gates/update/1
  def update
    @gate.attributes = gate_params
    terminal = Terminal.where(id: gate_params[:terminal_id]).first
    @gate.terminal = terminal

    respond_to do |format|
      if @gate.save()
        format.html { redirect_to action: :show, id: @gate.id, notice: 'Выход успешно обновлён.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # POST /gates/destroy/1
  def destroy
    @gate.destroy
    respond_to do |format|
      format.html { redirect_to action: :index, controller: :terminals }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gate
      @gate = Gate.includes(:terminal).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gate_params
      params.require(:gate).permit(:code, :terminal_id)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

class PassengerTicketsController < ApplicationController
  before_action :set_passenger_ticket, only: [:show, :edit, :update, :destroy]
  before_action :get_pass_fl, only: [:new, :create, :edit, :update]
  before_action :check_params, only: [:new]

  # GET /passenger_tickets
  # GET /passenger_tickets.json
  def index
    @passenger_tickets = PassengerTicket.all
  end

  # GET /passenger_tickets/1
  # GET /passenger_tickets/1.json
  def show
  end

  # GET /passenger_tickets/new
  def new
    @passenger_ticket = PassengerTicket.new()
  end

  # GET /passenger_tickets/1/edit
  def edit
    @flight = @passenger_ticket.ticket.flight.id
  end

  # POST /passenger_tickets
  # POST /passenger_tickets.json
  def create
    @passenger_ticket = PassengerTicket.new(passenger_ticket_params)

    tik = Ticket.where(seat_num: @seat_num).first
    pas = Passenger.where(id: params[:passenger_id]).first
    @passenger_ticket.ticket    = tik
    @passenger_ticket.passenger = pas

    respond_to do |format|
      if @passenger_ticket.save
        format.html { redirect_to @passenger_ticket, notice: 'Passenger ticket was successfully created.' }
      else
        flash[:notice] = "Пассажир уже зарегистрирован на рейс."
        format.html { redirect_to controller: :flights, action: :index, is_departure: true}
      end
    end
  end

  # PATCH/PUT /passenger_tickets/1
  # PATCH/PUT /passenger_tickets/1.json
  def update
    @passenger_ticket.attributes = passenger_ticket_params

    tik = Ticket.where(seat_num: @seat_num).first
    @passenger_ticket.ticket    = tik

    respond_to do |format|
      if @passenger_ticket.update(passenger_ticket_params)
        format.html { redirect_to @passenger_ticket, notice: 'Passenger ticket was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /passenger_tickets/1
  # DELETE /passenger_tickets/1.json
  def destroy
    @passenger_ticket.destroy
    respond_to do |format|
      format.html { redirect_to passenger_tickets_url, notice: 'Passenger ticket was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_passenger_ticket
      @passenger_ticket = PassengerTicket.find(params[:id])
    end

    def get_pass_fl
        @seat_num  = params[:seat_num]
        @passenger = params[:cid]
        @flight    = params[:fid]
    end

    def check_params
      if !params.has_key?(:cid) && params.has_key?(:fid)
        flash[:params] = "Необходимо ввести данные"
        redirect_to controller: :passengers, action: :new, fid: params[:fid]
      elsif !params.has_key?(:fid)
        flash[:params] = "Необходимо выбрать рейс"
        redirect_to controller: :flights, action: :index, is_departure: true
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def passenger_ticket_params
      params.require(:passenger_ticket).permit(:fid, :cid, :seat_num, :passenger_id, :ticket_id, :ttype)
    end
end

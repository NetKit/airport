# Данный контроллер пока не реализован
class InternationalNamesController < ApplicationController
  before_action :set_international_name, only: [:show, :edit, :update, :destroy]

  # GET /international_names
  # GET /international_names.json
  def index
    @international_names = InternationalName.all
  end

  # GET /international_names/1
  # GET /international_names/1.json
  def show
  end

  # GET /international_names/new
  def new
    @international_name = InternationalName.new
  end

  # GET /international_names/1/edit
  def edit
  end

  # POST /international_names
  # POST /international_names.json
  def create
    @international_name = InternationalName.new(international_name_params)

    respond_to do |format|
      if @international_name.save
        format.html { redirect_to @international_name, notice: 'International name was successfully created.' }
        format.json { render action: 'show', status: :created, location: @international_name }
      else
        format.html { render action: 'new' }
        format.json { render json: @international_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /international_names/1
  # PATCH/PUT /international_names/1.json
  def update
    respond_to do |format|
      if @international_name.update(international_name_params)
        format.html { redirect_to @international_name, notice: 'International name was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @international_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /international_names/1
  # DELETE /international_names/1.json
  def destroy
    @international_name.destroy
    respond_to do |format|
      format.html { redirect_to international_names_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_international_name
      @international_name = InternationalName.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def international_name_params
      params.require(:international_name).permit(:name, :language_id, :term_id, :term_type)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

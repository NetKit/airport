class AirportsController < ApplicationController
  before_action :set_airport, only: [:show, :edit, :update, :destroy]

  # GET /airports/index
  def index
    @airports = Airport.includes(city: [:country]).load
  end

  # GET /airports/show/1
  def show
  end

  # GET /airports/new
  def new
    @airport = Airport.new
  end

  # GET /airports/edit/1
  def edit
  end

  # POST /airports/create
  def create
    @airport = Airport.new(airport_params)
    city = City.where(id: airport_params['city_id']).first
    @airport.city = city

    respond_to do |format|
      if @airport.save
        format.html { redirect_to action: :show, id: @airport.id, notice: 'Аэропорт успешно создан.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /airports/update
  def update
    @airport.attributes = airport_params
    city = City.where(id: airport_params['city_id']).first
    @airport.city = city

    respond_to do |format|
      if @airport.save
        format.html { redirect_to action: :show, id: @airport.id, notice: 'Аэропорт успешно обновлён.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /airports/destroy/1
  def destroy
    @airport.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_airport
      @airport = Airport.includes(city: [:country]).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def airport_params
      params.require(:airport).permit(:name, :code, :city_id)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

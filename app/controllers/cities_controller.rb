class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :edit, :update, :destroy]

  # GET /cities/index
  def index
    @cities = City.all
  end

  # GET /cities/show/1
  def show
  end

  # GET /cities/new
  def new
    @city = City.new
    if params.has_key?('country_id')
      country = Country.where(id: params['country_id'].to_i).first
      @city.country = country
    end
  end

  # GET /cities/edit/1
  def edit
  end

  # POST /cities/create
  def create
    @city = City.new(city_params)
    country = Country.where(id: city_params['country_id'].to_i).first
    @city.country = country

    respond_to do |format|
      if @city.save
        format.html { redirect_to action: :show, id: @city.id, notice: 'Город успешно создан' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /cities/update
  def update
    @city.attributes = city_params
    country = Country.where(id: city_params['country_id'].to_i).first
    @city.country = country

    respond_to do |format|
      if @city.save
        format.html { redirect_to action: :show, id: @city.id, notice: 'Город успешно отредактирован.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /cities/destroy/1
  def destroy
    @city.destroy
    respond_to do |format|
      format.html do 
        if params.has_key?('country_action')
          redirect_to action: :index, controller: :countries
        else
          redirect_to action: :index
        end
      end        
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_params
      params.require(:city).permit(:name, :country_id, :code)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

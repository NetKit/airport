class LanguagesController < ApplicationController
  before_action :set_language, only: [:show, :edit, :update, :destroy]

  # GET /languages/index
  def index
    @languages = Language.all
  end

  # GET /languages/show/1
  def show
  end

  # GET /languages/new
  def new
    @language = Language.new
  end

  # GET /languages/edit/1
  def edit
  end

  # POST /languages/create
  def create
    @language = Language.new(language_params)

    respond_to do |format|
      if @language.save
        format.html { redirect_to action: :show, id: @language.id, notice: 'Язык удачно создан.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /languages/update
  def update
    respond_to do |format|
      if @language.update(language_params)
        format.html { redirect_to action: :show, id: @language.id, notice: 'Язык удачно обновлён.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /languages/destroy/1
  def destroy
    @language.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_language
      @language = Language.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def language_params
      params.require(:language).permit(:name)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

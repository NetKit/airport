class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies/index
  def index
    @companies = Company.all
  end

  # GET /companies/show/1
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/edit/1
  def edit
  end

  # POST /companies/create
  def create
    @company = Company.new(company_params)
    country = Country.where(id: company_params['country_id'].to_i).first
    @company.country = country

    respond_to do |format|
      if @company.save
        format.html { redirect_to action: :show, id: @company, 
          notice: 'Авиакомпания успешно создана.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /companies/update
  def update
    @company.attributes = company_params
    country = Country.where(id: company_params['country_id'].to_i).first
    @company.country = country

    respond_to do |format|
      if @company.save
        format.html { redirect_to action: :show, id: @company.id, 
          notice: 'Авиакомпания успешно обновлена.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /companies/destroy/1
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :code, :country_id)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

class FlightsController < ApplicationController
  before_action :set_flight, only: [:show, :edit, :update, :destroy, :passenger_list]
  before_action :set_dep, only: [:index]
  before_action :set_list, only: [:passenger_list]

  # GET /flights/index
  def index
    if params.has_key?(:is_departure)
      @flights = Flight.includes(:company, :terminal, :gate,
        :registration_desk, airport: [:city]).where(is_departure: params[:is_departure]).load
    else
      @flights = Flight.includes(:company, :terminal, :gate,
        :registration_desk, airport: [:city]).load
    end
  end

  # GET /flights/edit/1
  def show
  end

  # GET /flights/new
  def new
    @flight = Flight.new
  end

  def passengers_list
    @plist = Passenger.includes(passenger_tickets: {ticket: [:flight]}).where(flight: {id: @flight.id})
  end

  # GET /flights/edit/1
  def edit
  end

  # POST /flights/create
  def create
    @flight = Flight.new(flight_params)
    unless flight_params.has_key?('is_departure')
      @flight.is_departure = false
    else
      @flight.is_departure = true
    end
    @flight.another_airport_date_time = convert_date(
      flight_params[:another_airport_date_time])
    @flight.this_airport_date_time = convert_date(
      flight_params[:this_airport_date_time])
    # Присваивать идентификаторы ассоциациям плохо!
    cmp = Company.where(id: @flight.company_id).first
    @flight.company = cmp
    apt = Airport.where(id: @flight.airport_id).first
    @flight.airport = apt
    trm = Terminal.where(id: @flight.terminal_id).first
    @flight.terminal = trm
    rd = RegistrationDesk.where(id: @flight.registration_desk_id).first
    @flight.registration_desk = rd
    gt = Gate.where(id: @flight.gate).first
    @flight.gate = gt

    respond_to do |format|
      if @flight.save
        format.html { redirect_to(action: :edit, id: @flight.id,
          notice: 'Рейс успешно создан.') }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /flights/update/1
  def update
    @flight.attributes = flight_params
    unless flight_params.has_key?('is_departure')
      @flight.is_departure = false
    else
      @flight.is_departure = true
    end
    @flight.another_airport_date_time = convert_date(
      flight_params[:another_airport_date_time])
    @flight.this_airport_date_time = convert_date(
      flight_params[:this_airport_date_time])
    # Присваивать идентификаторы ассоциациям плохо!
    cmp = Company.where(id: @flight.company_id).first
    @flight.company = cmp
    apt = Airport.where(id: @flight.airport_id).first
    @flight.airport = apt
    trm = Terminal.where(id: @flight.terminal_id).first
    @flight.terminal = trm
    rd = RegistrationDesk.where(id: @flight.registration_desk_id).first
    @flight.registration_desk = rd
    gt = Gate.where(id: @flight.gate).first
    @flight.gate = gt

    respond_to do |format|
      if @flight.save
        format.html { redirect_to action: :show, id: @flight.id, notice: 'Рейс успешно обновлён.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /flights/destroy/1
  def destroy
    @flight.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flight
      @flight = Flight.includes(:company, :terminal, :gate,
        :registration_desk, airport: [:city]).where(id: params[:id].to_i).first
    end

    def set_list
      @flight = Flight.includes(:company, :terminal, :gate,
      :registration_desk, airport: [:city]).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def flight_params
      params.require(:flight).permit(:code, :eco_cost, :prem_cost, :buis_cost, :is_departure, :terminal_id, :registration_desk_id, :gate_id, :airport_id, {this_airport_date_time: [:month, :year, :day, :hour, :minute]}, {another_airport_date_time: [:month, :year, :day, :hour, :minute]}, :company_id)
    end

    def set_dep
      @is_dep = params[:is_departure]
    end

    def convert_date(hsh)
      Time.local(hsh[:year].to_i, hsh[:month].to_i, hsh[:day].to_i,
        hsh[:hour].to_i, hsh[:minute].to_i).to_datetime
    end

    def permit?(aname)
      return true if aname == 'index' or aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

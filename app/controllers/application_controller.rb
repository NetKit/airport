class ApplicationController < ActionController::Base
  OPERATOR_ROLE = Role.where(name: 'operator').first 
  ADMIN_ROLE = Role.where(name: 'admin').first 
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :check_user, :check_permissions
  before_action :set_permission

  private

  def check_user()
    if params.has_key?('auth_login') and params.has_key?('auth_password')
      @user = User.check_user(params['auth_login'], params['auth_password'])
      if @user.nil?
        redirect_to(root_path)
      else
        session[:user] = @user.login
        session[:password] = @user.password
      end
    elsif session[:user].nil?
      @user = nil
    else
      @user = User.load_user(session[:user], session[:password])      
    end
  end

  def check_permissions()
    unless permit_action?()
      redirect_to(root_path)
    end
  end

  def permit_action?()
    permit?(action_name)
  end
    
  def permit?(aname)
    true
  end
  
  def set_permission
    @can_edit = permit?('edit')
  end
end

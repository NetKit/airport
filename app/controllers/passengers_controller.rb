class PassengersController < ApplicationController
  before_action :set_passenger, only: [:show, :edit, :update, :destroy]
  before_action :transit_flight, only: [:new, :create]

  # GET /passengers
  # GET /passengers.json
  def index
    @passengers = Passenger.all
  end

  # GET /passengers/1
  # GET /passengers/1.json
  def show
  end

  # GET /passengers/new
  def new
    @passenger = Passenger.new
  end

  # GET /passengers/1/edit
  def edit
  end

  # POST /passengers
  def create
    @passenger = Passenger.new(passenger_params)

    puts @flight

    respond_to do |format|
      if (p = Passenger.where(passport_num: @passenger.passport_num, foreign_passport: @passenger.foreign_passport).pluck(:id)).size > 0
        format.html { redirect_to controller: :passenger_tickets, action: :new, cid: p.first, fid: @fid }
      elsif @passenger.save
        format.html { redirect_to controller: :passenger_tickets, action: :new, cid: @passenger.id, fid: @fid }
      else
        format.html { render :new}
      end
    end
  end

  # PATCH/PUT /passengers/1
  # PATCH/PUT /passengers/1.json
  def update
    respond_to do |format|
      if @passenger.update(passenger_params)
        format.html { redirect_to @passenger, notice: 'Пассажир был успешно создан' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /passengers/1
  # DELETE /passengers/1.json
  def destroy
    @passenger.destroy
    respond_to do |format|
      format.html { redirect_to passengers_url, notice: 'Пассажир был успешно удален' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_passenger
      @passenger = Passenger.find(params[:id])
    end

    def transit_flight
      if params.has_key?(:fid)
        @fid = params[:fid]
      else
        flash[:params] = "Необходимо выбрать рейс!"
        redirect_to controller: :flights, action: :index, is_departure: true
      end
      @fid = params[:fid]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def passenger_params
      params.require(:passenger).permit(:pnger, :fid ,:name, :lname, :sname, :passport_num, :foreign_passport, :citizenship, :sex, :bdate)
    end
end

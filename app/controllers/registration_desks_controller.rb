class RegistrationDesksController < ApplicationController
  before_action :set_registration_desk, only: [:show, :edit, :update, :destroy]

  # GET /registration_desks/show/1
  def show
  end

  # GET /registration_desks/new
  def new
    @registration_desk = RegistrationDesk.new
    terminal = Terminal.where(id: params[:terminal_id]).first
    @registration_desk.terminal = terminal
  end

  # GET /registration_desks/edit/1
  def edit
  end

  # POST /registration_desks/create
  def create
    @registration_desk = RegistrationDesk.new(registration_desk_params)
    terminal = Terminal.where(id: registration_desk_params[:terminal_id]).first
    @registration_desk.terminal = terminal

    respond_to do |format|
      if @registration_desk.save
        format.html { redirect_to action: :show, id: @registration_desk.id, notice: 'Регистрационная стойка успешно создана.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /registration_desks/update
  def update
    @registration_desk.attributes = registration_desk_params
    terminal = Terminal.where(id: registration_desk_params[:terminal_id]).first
    @registration_desk.terminal = terminal
    respond_to do |format|
      if @registration_desk.save()
        format.html { redirect_to action: :show, id: @registration_desk.id, notice: 'Регистрационная стойка успешно обновлена.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /registration_desks/destroy/1
  def destroy
    @registration_desk.destroy
    respond_to do |format|
      format.html { redirect_to action: :index, controller: :terminals }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_registration_desk
      @registration_desk = RegistrationDesk.includes(:terminal).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def registration_desk_params
      params.require(:registration_desk).permit(:code, :terminal_id)
    end

    def permit?(aname)
      return true if aname == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

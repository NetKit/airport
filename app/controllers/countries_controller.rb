class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]

  # GET /countries/index
  def index
    @countries = Country.includes(:cities, :language).load
  end

  # GET /countries/show/1
  def show
  end

  # GET /countries/new
  def new
    @country = Country.new
  end

  # GET /countries/edit/1
  def edit
  end

  # POST /countries/create
  def create
    @country = Country.new(country_params)
    language = Language.where(id: @country.language_id).first
    @country.language = language

    respond_to do |format|
      if @country.save
        format.html { redirect_to(action: :show, id: @country.id, 
            notice: 'Страна успешно создана.') }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # POST /countries/update
  def update
    @country.attributes = country_params
    language = Language.where(id: @country.language_id).first
    @country.language = language

    respond_to do |format|
      if @country.save
        format.html { redirect_to(action: :show, id: @country, 
            notice: 'Страна успешно обновлена.') }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # GET /countries/destroy/1
  def destroy
    @country.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.includes(:cities, :language).where(id: params[:id].to_i).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:name, :code, :language_id)
    end

    def permit?(aname)
      return true if action_name == 'show'
      return true if !@user.nil? and @user.roles.include?(OPERATOR_ROLE)
      return false
    end
end

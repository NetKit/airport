class Language < ActiveRecord::Base
  has_many :countries, dependent: :destroy
  has_many :international_names, dependent: :destroy
  validates :name, presence: true, uniqueness: true
end

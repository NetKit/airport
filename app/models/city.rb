class City < ActiveRecord::Base
  belongs_to :country
  has_many :airports, dependent: :destroy
  has_many :international_names, as: :term, dependent: :destroy

  validates :name, presence: true
  validates :country_id, presence: true
  validates :code, presence: true, uniqueness: true, 
    length: {maximum: 4}
end

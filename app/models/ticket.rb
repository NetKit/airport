class Ticket < ActiveRecord::Base
  belongs_to :flight
  has_one    :passenger_ticket, dependent: :destroy

  validates :seat_class, :price, :seat_num, presence: true
  validates :seat_class,                    inclusion: {in: ["Эконом", "Премиум", "Бизнес"]}
  validates :price,                         numericality: {greater_than_or_equal_to: 0.00, message: "не может быть отрицательной"}
  validates :seat_num,                      format: {with: /\A[A-D]\d{1,2}\z/}
  validates :seat_num,                      uniqueness: {scope: :flight}

  def getCompanyName
    Company.includes(:flights).where(flights: {id: self.flight.id}).pluck(:name)
  end

  def getAirportName
    Airport.includes(:flights).where(flights: {id: self.flight.id}).pluck(:name)
  end

  def getFlightCode
    Flight.where(id: self.flight.id).pluck(:code)
  end

end

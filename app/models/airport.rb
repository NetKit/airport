class Airport < ActiveRecord::Base
  belongs_to :city
  has_many :flights, dependent: :destroy
  has_many :international_names, as: :term, dependent: :destroy
  

  validates :name, presence: true
  validates :city_id, presence: true
  validates :code, presence: true, uniqueness: true, 
    length: {maximum: 4}
end

require 'digest/md5'

class User < ActiveRecord::Base
  has_and_belongs_to_many :roles
  validates :login, uniqueness: true, presence: true, length: {maximum: 32}
  validates :password, presence: true
  
  before_validation :crypt_password
  
  def User.check_user(login, password)
    cpassword =  Digest::MD5.hexdigest(password)
    User.where('login = ? AND password = ?', 
      login, cpassword).includes(:roles).first    
  end
  
  def User.load_user(login, password)
    User.where('login = ? AND password = ?', 
      login, password).includes(:roles).first    
  end

  protected
  def crypt_password
    self.password = Digest::MD5.hexdigest(self.password)
  end
end

class Country < ActiveRecord::Base
  belongs_to :language
  has_many :companies, dependent: :destroy
  has_many :cities, dependent: :destroy
  has_many :international_names, as: :term, dependent: :destroy

  validates :name, presence: true
  validates :language_id, presence: true
  validates :code, presence: true, uniqueness: true, 
    length: {maximum: 4}
end

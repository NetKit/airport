class Company < ActiveRecord::Base
  belongs_to :country, dependent: :destroy
  has_many :flights, dependent: :destroy

  validates :name, presence: true
  validates :country_id, presence: true
  validates :code, presence: true, uniqueness: true, 
    length: {maximum: 4}
end

class InternationalName < ActiveRecord::Base
  belongs_to :language
  belongs_to :term, polymorphic: true

  validates :language_id, presence: true
  validates :term_id, presence: true
  validates :term_type, presence: true
  validates :name, presence: true
end

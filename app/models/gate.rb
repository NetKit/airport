class Gate < ActiveRecord::Base
  belongs_to :terminal
  has_many :flights, dependent: :nullify

  validates :code, presence: true, uniqueness: true, 
    length: {maximum: 4}
  validates :terminal_id, presence: true
end

class Terminal < ActiveRecord::Base
  has_many :gates, dependent: :destroy
  has_many :registration_desks, dependent: :destroy
  has_many :flights, dependent: :destroy
  
  validates :code, presence: true, uniqueness: true, 
    length: {maximum: 4}

  def <=>(b)
    return 0 unless b.kind_of?(self.class)
    return self.code <=> b.code
  end
end

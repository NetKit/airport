class Plane < ActiveRecord::Base
  after_validation  :correCt
  belongs_to        :company
  has_many          :flights

  validates :num, :model, :type, presence: true
  validates :amount_of_seats, :floors, :rows, :buis_st, :eco_st, :prem_st, presence: true, if: "type == 'Пассажирский'"
  validates :type, inclusion: {in: ["Пассажирский","Грузовой"]}
  validates :buis_st, :eco_st, :prem_st, numericality: {only_integer: true}
  validates :amount_of_seats, :rows, numericality: {only_intgere: true, greater_than: 0}

  protected

  def correCt
    if :type == "Грузовой"
      :rows            = nil
      :buis_st         = nil
      :eco_st          = nil
      :prem_st         = nil
      :amount_of_seats = nil
    end
  end
end

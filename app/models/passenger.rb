class Passenger < ActiveRecord::Base
  has_many :passenger_tickets, dependent: :destroy

  validates :name, :sname, :passport_num, :foreign_passport,
            :citizenship, :sex, :bdate, presence: true
  validates :name, :sname,              format: {with: /\A[A-Z][a-z]+\z/,     message: "Только латинские буквы" }
  validates :lname,                     format: {with: /\A[A-Z][a-z]+\z/,     message: "Только латинские буквы" }, if: "!lname.blank?"
  validates :citizenship,               format: {with: /\A[A-Z][A-Za-z ]+\z/, message: "Только латинские буквы" }
  validates :passport_num,              format: {with: /\A[0-9]{10}\z/,       message: "Только цифры" }
  validates :foreign_passport,          format: {with: /\A[0-9]{9}\z/,        message: "Только цифпы" }
  validates :sex,                       inclusion: {in: ["м","ж"],            message: "Может быть только м или ж"}
  validates :foreign_passport,          uniqueness: true
  validates :passport_num,              uniqueness: true

end

class Flight < ActiveRecord::Base
  after_save :init_seats
  belongs_to :terminal
  belongs_to :registration_desk
  belongs_to :gate
  belongs_to :airport
  belongs_to :company
  has_many   :tickets, dependent: :destroy
  belongs_to :palne

  validates :company_id,                        presence: true
  validates :airport_id,                        presence: true
  validates :terminal_id,                       presence: true
  validates :eco_cost, :prem_cost, :buis_cost,  presence: true
  validates :eco_cost, :prem_cost, :buis_cost,  numericality: {greater_than_or_equal_to: 0.00}
  validates :code, presence: true,           uniqueness: true,
   length: {maximum: 8}
  validates :is_departure,                      inclusion: {in: [true, false]},
    allow_nil: false
  validates :this_airport_date_time,            presence: true
  validates :another_airport_date_time,         presence: true

  def show_terminal()
    self.terminal.nil? ? '' : self.terminal.code
  end

  def show_registration_desk()
    self.registration_desk.nil? ? '' : self.registration_desk.code
  end

  def show_gate()
    self.gate.nil? ? '' : self.gate.code
  end

  def show_is_departure()
    self.is_departure == true ? 'вылетает' : 'прилетает'
  end

  def qregisteredOnFlight
    PassengerTicket.includes(ticket: [:flight]).where(flights: {id: self.id}).count
  end

  protected

  def init_seats
    1.upto(5) do |i|
      ("A".."C").each do |j|
        Ticket.create(seat_num: j + i.to_s, seat_class: "Бизнес", price: self.buis_cost, flight_id: self.id)
      end
    end
    6.upto(10) do |i|
      ("A".."D").each do |j|
        Ticket.create(seat_num: j + i.to_s, seat_class: "Премиум", price: self.prem_cost, flight_id: self.id)
      end
    end
    11.upto(60) do |i|
      ("A".."F").each do |j|
        Ticket.create(seat_num: j + i.to_s, seat_class: "Эконом", price: self.eco_cost, flight_id: self.id)
      end
    end
  end
end

class UniqPassenger < ActiveModel::Validator
  def validate(record)
    if record.new_record? && Passenger.includes(passenger_tickets: {ticket: [:flight]}).where(passenger_tickets: { passenger: record.passenger}).size > 0
      record.errors[:passenger] << "Такой пассажир уже зарегистрирован на рейс!"
    end
  end
end

class PassengerTicket < ActiveRecord::Base
  before_validation :genNumber
  belongs_to        :passenger
  belongs_to        :ticket

  validates :e_num, :ttype,  presence: true
  validates :ttype,          inclusion: {in: ["Детский","Взрослый"],  message: "Такой тип билета не существует" }
  validates :e_num,          format: {with: /\A[0-9]{13}\z/ }
  validates_with UniqPassenger

  protected

  def genNumber
    if new_record?
      c = Company.includes(flights: {tickets: [:passenger_ticket]}).where(passenger_tickets: {id: self.id}).limit(1).pluck('companies.id')
      e = (PassengerTicket.includes(ticket: {flight: [:company]}).where(companies: {id: c}).count + 1).to_s
      self.e_num = "0" * (13 - e.size) + e
    end
  end
end

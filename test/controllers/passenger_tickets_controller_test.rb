require 'test_helper'

class PassengerTicketsControllerTest < ActionController::TestCase
  setup do
    @passenger_ticket = passenger_tickets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:passenger_tickets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create passenger_ticket" do
    assert_difference('PassengerTicket.count') do
      post :create, passenger_ticket: { e_num: @passenger_ticket.e_num, passenger_id: @passenger_ticket.passenger_id, pnr: @passenger_ticket.pnr, ticket_id: @passenger_ticket.ticket_id, type: @passenger_ticket.type }
    end

    assert_redirected_to passenger_ticket_path(assigns(:passenger_ticket))
  end

  test "should show passenger_ticket" do
    get :show, id: @passenger_ticket
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @passenger_ticket
    assert_response :success
  end

  test "should update passenger_ticket" do
    patch :update, id: @passenger_ticket, passenger_ticket: { e_num: @passenger_ticket.e_num, passenger_id: @passenger_ticket.passenger_id, pnr: @passenger_ticket.pnr, ticket_id: @passenger_ticket.ticket_id, type: @passenger_ticket.type }
    assert_redirected_to passenger_ticket_path(assigns(:passenger_ticket))
  end

  test "should destroy passenger_ticket" do
    assert_difference('PassengerTicket.count', -1) do
      delete :destroy, id: @passenger_ticket
    end

    assert_redirected_to passenger_tickets_path
  end
end

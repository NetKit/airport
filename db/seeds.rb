# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

langs = Language.create([{name: 'Русский'}, {name: 'Английский'},
    {name: 'Испанский'}])
countries = Country.create([{name: 'Россия', code: 'RU',
      language: langs[0]}, {name: 'Испания', code: 'ES',
      language: langs[2]}])
cities = City.create([{name: 'Москва', code: 'MOW',
      country: countries[0]}, {name: 'Барселона', code: 'BCN',
      country: countries[1]}])
aports = Airport.create([{name: 'Внуково', code: 'VKO',
      city: cities[0]}, {name: 'Эль-Прат', code: 'IATA',
      city: cities[1]}])
terms = Terminal.create([{code: 'A'}, {code: 'B'}])
rds = RegistrationDesk.create([{code: '1', terminal: terms[0]},
    {code: '2', terminal: terms[0]},
    {code: '1', terminal: terms[1]},
    {code: '2', terminal: terms[1]}])
gates = Gate.create([{code: '1', terminal: terms[0]},
                     {code: '2', terminal: terms[0]},
                     {code: '1', terminal: terms[1]},
                     {code: '2', terminal: terms[1]}])
cmps = Company.create([{name: 'Трансаэро', code: 'UN',
      country: countries [0]}])
Flight.create([{company: cmps[0], gate: gates[0],
    registration_desk: rds[1], terminal: terms[0],
    airport: aports[0], code: '0021',
    this_airport_date_time: Time.local(2013, 9, 21, 6, 25).to_datetime,
    another_airport_date_time: Time.local(2013, 9, 21, 7, 45).to_datetime,
    is_departure: true, eco_cost: 15.30, prem_cost: 20.30, buis_cost: 30.50},
    {company: cmps[0], gate: gates[2], registration_desk: rds[2],
      terminal: terms[1], airport: aports[1], code: '0432',
    this_airport_date_time: Time.local(2013, 9, 21, 23, 0).to_datetime,
    another_airport_date_time: Time.local(2013, 9, 21, 16, 50).to_datetime,
    is_departure: false, eco_cost: 15.30, prem_cost: 20.30, buis_cost: 30.50}])
InternationalName.create([{name: 'Barcelona', language: langs[1],
  term: cities[1]}, {name: 'Barcelona', language: langs[2],
  term: cities[1]}, {name: 'El Prat', language: langs[1],
  term: aports[1]}, {name: 'El Prat', language: langs[2],
  term: aports[1]}, {name: 'Spain', language: langs[1],
  term: countries[1]}, {name: 'España', language: langs[2],
  term: countries[1]}])
rs = Role.create([{name: 'admin', info: 'Администратор системы'},
  {name: 'operator', info: 'Оператор системы'}])
us = User.create([{login: 'admin', password: 'qwerty', info: 'Админ'},
    {login: 'op', password: 'qwerty', info: 'Оператор'}])
us[0].roles << rs[0]
us[1].roles << rs[1]

p = Passenger.create(name: "Nikita", sname: "Salkin", lname: "Yuryevich", passport_num: "4321123456", foreign_passport: "111234567", citizenship: "Russian Federation", sex: "м", bdate: Date.new(1995,10,24))

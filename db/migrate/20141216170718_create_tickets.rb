class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :seat_class,             null: false                             # класс места (бизнес, премиум, эконом)
      t.decimal :price,                 precision: 8, scale: 2, null: false, default: 0.00
      t.references :flight,             null: false
      t.string :seat_num,               null: false                             # номер места(A-K, 1 - 51)
      t.index [:seat_num, :flight_id],  unique: true

      t.timestamps
    end

    add_foreign_key :tickets, :flights
    
    reversible do |i|
      i.up do
        execute "ALTER TABLE Tickets ADD CHECK (price >= 0.0)"
        execute "ALTER TABLE Tickets ADD CHECK (seat_class IN ('Эконом','Премиум','Бизнес'))"
      end
    end
  end
end

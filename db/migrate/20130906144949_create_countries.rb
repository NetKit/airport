class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.text        :name,      null: false
      t.string      :code,      null: false, limit: 4
      t.references  :language,  index: true, null: false
      t.index       :code,      unique: true

      t.timestamps
    end

    add_foreign_key :countries, :languages

  end
end

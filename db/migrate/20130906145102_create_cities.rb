class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.text       :name,     null: false
      t.string     :code,     null: false
      t.references :country,  index: true, null: false

      t.timestamps
    end

    add_foreign_key :cities, :countries

  end
end

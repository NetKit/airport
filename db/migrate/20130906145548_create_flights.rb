class CreateFlights < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.string      :code,                      null: false, limit: 8
      t.boolean     :is_departure,              null: false, default: true
      t.references  :terminal,                  index: true, null: false
      t.references  :registration_desk,         index: true
      t.references  :gate,                      index: true
      t.references  :airport,                   index: true, null: false
      t.timestamp   :this_airport_date_time,    null: false
      t.timestamp   :another_airport_date_time, null: false
      t.references  :company,                   index: true, null: false
      t.index       :code,                      unique: true

      t.timestamps
    end

    add_foreign_key :flights, :terminals
    add_foreign_key :flights, :registration_desks
    add_foreign_key :flights, :gates
    add_foreign_key :flights, :airports
    add_foreign_key :flights, :companies

  end
end

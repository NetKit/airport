class CreatePlanes < ActiveRecord::Migration
  def change
    create_table :planes do |t|
      t.integer :num,     null: false
      t.string :model,    null: false
      t.integer :floors
      t.integer :rows
      t.integer :buis_st
      t.integer :prem_st
      t.integer :eco_st
      t.string :type,     null: false
      t.integer :amount_of_seats
      t.index [:num, :model], unique: true
      t.references :company

      t.timestamps null: false
    end

    add_foreign_key :planes, :companies

  end
end

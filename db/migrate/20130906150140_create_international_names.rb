class CreateInternationalNames < ActiveRecord::Migration
  def change
    create_table :international_names do |t|
      t.text :name, null: false
      t.references :language, index: true, null: false
      t.references :term,     polymorphic: true, index: true, null: false

      t.timestamps
    end

    add_foreign_key :international_names, :languages
    
  end
end

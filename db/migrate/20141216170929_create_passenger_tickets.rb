class CreatePassengerTickets < ActiveRecord::Migration
  def change
    create_table :passenger_tickets do |t|
      t.string      :e_num,                   null: false, limit: 13              # номер электронного билета
      t.references  :passenger,               null: false
      t.references  :ticket,                  null: false
      t.index       :ticket_id,               unique: true
      t.string      :ttype,                   null: false               # тип (детский, взрослый)
      t.timestamps
    end

    add_foreign_key :passenger_tickets, :passengers
    add_foreign_key :passenger_tickets, :tickets

    reversible do |i|
      i.up do
        execute "ALTER TABLE passenger_tickets ADD CHECK (ttype IN ('Взрослый', 'Детский'))"
      end
    end
  end
end

class CreateRegistrationDesks < ActiveRecord::Migration
  def change
    create_table :registration_desks do |t|
      t.string      :code,     null: false, limit: 4
      t.references  :terminal, index: true, null: false

      t.timestamps
    end

    add_foreign_key :registration_desks, :terminals

  end
end

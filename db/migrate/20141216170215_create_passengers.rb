class CreatePassengers < ActiveRecord::Migration
  def change
    create_table :passengers do |t|
      t.string :name,               null: false
      t.string :lname,              null: false
      t.string :sname,              null: false
      t.string :passport_num,       null: false
      t.string :foreign_passport,   null: false
      t.string :citizenship,        null: false
      t.string :sex,                null: false
      t.date   :bdate,              null: false
      t.index  :passport_num,       unique: true
      t.index  :foreign_passport,   unique: true

      t.timestamps
    end
    reversible do |i|
      i.up do
        execute "ALTER TABLE passengers ADD CHECK (sex IN ('м', 'ж'))"
      end
    end
  end
end

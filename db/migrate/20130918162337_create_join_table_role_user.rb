class CreateJoinTableRoleUser < ActiveRecord::Migration
  def change
    create_join_table :roles, :users do |t|
      # t.index [:user_id, :role_id]
      # t.index [:role_id, :user_id]
    end

    add_foreign_key :roles_users, :roles
    add_foreign_key :roles_users, :users

    reversible do |dir|
      dir.up do
        execute "ALTER TABLE roles_users ADD PRIMARY KEY (role_id, user_id)"
      end
    end
  end
end

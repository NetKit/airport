class CreateTerminals < ActiveRecord::Migration
  def change
    create_table :terminals do |t|
      t.string  :code, null: false, limit: 4
      t.index   :code, unique: true

      t.timestamps
    end
  end
end

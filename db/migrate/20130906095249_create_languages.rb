class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
      t.text  :name, null: false
      t.index :name, unique: true

      t.timestamps
    end
  end
end

class CreateGates < ActiveRecord::Migration
  def change
    create_table :gates do |t|
      t.string      :code,     null: false, limit: 4
      t.references  :terminal, index: true, null: false

      t.timestamps
    end

    add_foreign_key :gates, :terminals

  end
end

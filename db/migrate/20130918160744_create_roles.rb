class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string  :name, null: false, limit: 16
      t.index   :name, unique: true
      t.text    :info

      t.timestamps
    end
  end
end

class AddCostColumnToFlights < ActiveRecord::Migration
  def change
    add_column :flights, :eco_cost,  :decimal, precision: 8, scale: 2, null: false, default: 0.0
    add_column :flights, :prem_cost, :decimal, precision: 8, scale: 2, null: false, default: 0.0
    add_column :flights, :buis_cost, :decimal, precision: 8, scale: 2, null: false, default: 0.0
    add_reference :flights, :plane,  index: true
    add_foreign_key :flights, :planes
=begin
    reversible do |i|
      i.up do
        execute "ALTER TABLE flights ADD FOREIGN KEY (plane_id) REFERENCES Planes(id)"
      end
    end
=end
  end
end

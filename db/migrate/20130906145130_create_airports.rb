class CreateAirports < ActiveRecord::Migration
  def change
    create_table :airports do |t|
      t.text        :name, null: false
      t.string      :code, null: false
      t.references  :city, index: true, null: false
      t.index       :code, unique: true

      t.timestamps
    end

    add_foreign_key :airports, :cities

  end
end

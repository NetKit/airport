class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.text        :name,    null: false
      t.string      :code,    null: false, limit: 4
      t.references  :country, index: true, null: false
      t.index       :code,    unique: true

      t.timestamps
    end

    add_foreign_key :companies, :countries

  end
end

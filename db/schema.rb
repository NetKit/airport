# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151221202025) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "airports", force: :cascade do |t|
    t.text     "name",       null: false
    t.string   "code",       null: false
    t.integer  "city_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "airports", ["city_id"], name: "index_airports_on_city_id", using: :btree
  add_index "airports", ["code"], name: "index_airports_on_code", unique: true, using: :btree

  create_table "cities", force: :cascade do |t|
    t.text     "name",       null: false
    t.string   "code",       null: false
    t.integer  "country_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["country_id"], name: "index_cities_on_country_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.text     "name",                 null: false
    t.string   "code",       limit: 4, null: false
    t.integer  "country_id",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "companies", ["code"], name: "index_companies_on_code", unique: true, using: :btree
  add_index "companies", ["country_id"], name: "index_companies_on_country_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.text     "name",                  null: false
    t.string   "code",        limit: 4, null: false
    t.integer  "language_id",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "countries", ["code"], name: "index_countries_on_code", unique: true, using: :btree
  add_index "countries", ["language_id"], name: "index_countries_on_language_id", using: :btree

  create_table "flights", force: :cascade do |t|
    t.string   "code",                      limit: 8,                                        null: false
    t.boolean  "is_departure",                                                default: true, null: false
    t.integer  "terminal_id",                                                                null: false
    t.integer  "registration_desk_id"
    t.integer  "gate_id"
    t.integer  "airport_id",                                                                 null: false
    t.datetime "this_airport_date_time",                                                     null: false
    t.datetime "another_airport_date_time",                                                  null: false
    t.integer  "company_id",                                                                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "eco_cost",                            precision: 8, scale: 2, default: 0.0,  null: false
    t.decimal  "prem_cost",                           precision: 8, scale: 2, default: 0.0,  null: false
    t.decimal  "buis_cost",                           precision: 8, scale: 2, default: 0.0,  null: false
    t.integer  "plane_id"
  end

  add_index "flights", ["airport_id"], name: "index_flights_on_airport_id", using: :btree
  add_index "flights", ["code"], name: "index_flights_on_code", unique: true, using: :btree
  add_index "flights", ["company_id"], name: "index_flights_on_company_id", using: :btree
  add_index "flights", ["gate_id"], name: "index_flights_on_gate_id", using: :btree
  add_index "flights", ["plane_id"], name: "index_flights_on_plane_id", using: :btree
  add_index "flights", ["registration_desk_id"], name: "index_flights_on_registration_desk_id", using: :btree
  add_index "flights", ["terminal_id"], name: "index_flights_on_terminal_id", using: :btree

  create_table "gates", force: :cascade do |t|
    t.string   "code",        limit: 4, null: false
    t.integer  "terminal_id",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "gates", ["terminal_id"], name: "index_gates_on_terminal_id", using: :btree

  create_table "international_names", force: :cascade do |t|
    t.text     "name",        null: false
    t.integer  "language_id", null: false
    t.integer  "term_id",     null: false
    t.string   "term_type",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "international_names", ["language_id"], name: "index_international_names_on_language_id", using: :btree
  add_index "international_names", ["term_type", "term_id"], name: "index_international_names_on_term_type_and_term_id", using: :btree

  create_table "languages", force: :cascade do |t|
    t.text     "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "languages", ["name"], name: "index_languages_on_name", unique: true, using: :btree

  create_table "passenger_tickets", force: :cascade do |t|
    t.string   "e_num",        limit: 13, null: false
    t.integer  "passenger_id",            null: false
    t.integer  "ticket_id",               null: false
    t.string   "ttype",                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "passenger_tickets", ["ticket_id"], name: "index_passenger_tickets_on_ticket_id", unique: true, using: :btree

  create_table "passengers", force: :cascade do |t|
    t.string   "name",             null: false
    t.string   "lname",            null: false
    t.string   "sname",            null: false
    t.string   "passport_num",     null: false
    t.string   "foreign_passport", null: false
    t.string   "citizenship",      null: false
    t.string   "sex",              null: false
    t.date     "bdate",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "passengers", ["foreign_passport"], name: "index_passengers_on_foreign_passport", unique: true, using: :btree
  add_index "passengers", ["passport_num"], name: "index_passengers_on_passport_num", unique: true, using: :btree

  create_table "planes", force: :cascade do |t|
    t.integer  "num",             null: false
    t.string   "model",           null: false
    t.integer  "floors"
    t.integer  "rows"
    t.integer  "buis_st"
    t.integer  "prem_st"
    t.integer  "eco_st"
    t.string   "type",            null: false
    t.integer  "amount_of_seats"
    t.integer  "company_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "planes", ["num", "model"], name: "index_planes_on_num_and_model", unique: true, using: :btree

  create_table "registration_desks", force: :cascade do |t|
    t.string   "code",        limit: 4, null: false
    t.integer  "terminal_id",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "registration_desks", ["terminal_id"], name: "index_registration_desks_on_terminal_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",       limit: 16, null: false
    t.text     "info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name"], name: "index_roles_on_name", unique: true, using: :btree

  create_table "roles_users", primary_key: "role_id", force: :cascade do |t|
    t.integer "user_id", null: false
  end

  create_table "terminals", force: :cascade do |t|
    t.string   "code",       limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "terminals", ["code"], name: "index_terminals_on_code", unique: true, using: :btree

  create_table "tickets", force: :cascade do |t|
    t.string   "seat_class",                                       null: false
    t.decimal  "price",      precision: 8, scale: 2, default: 0.0, null: false
    t.integer  "flight_id",                                        null: false
    t.string   "seat_num",                                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tickets", ["seat_num", "flight_id"], name: "index_tickets_on_seat_num_and_flight_id", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "login",      limit: 32, null: false
    t.text     "info"
    t.text     "password",              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree

  add_foreign_key "airports", "cities"
  add_foreign_key "cities", "countries"
  add_foreign_key "companies", "countries"
  add_foreign_key "countries", "languages"
  add_foreign_key "flights", "airports"
  add_foreign_key "flights", "companies"
  add_foreign_key "flights", "gates"
  add_foreign_key "flights", "planes"
  add_foreign_key "flights", "registration_desks"
  add_foreign_key "flights", "terminals"
  add_foreign_key "gates", "terminals"
  add_foreign_key "international_names", "languages"
  add_foreign_key "passenger_tickets", "passengers"
  add_foreign_key "passenger_tickets", "tickets"
  add_foreign_key "planes", "companies"
  add_foreign_key "registration_desks", "terminals"
  add_foreign_key "roles_users", "roles"
  add_foreign_key "roles_users", "users"
  add_foreign_key "tickets", "flights"
end
